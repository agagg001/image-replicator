import numpy as np
from numpy._typing import NDArray
from PIL import Image, ImageDraw

class Picture:
    def __init__(self, img_dimensions: tuple[int, int], num_triangles: int) -> None:
        self.img_width, self.img_height = img_dimensions
        self.content = None

        self.triangle_colors = np.random.randint(256, size=(num_triangles, 4), dtype=np.uint8)
        self.triangle_edges = self.initialize_edge_coords(num_triangles)


    def initialize_edge_coords(self, num_triangles: int) -> NDArray[np.uint16]:
        edges = np.empty((num_triangles, 6), dtype=np.uint16)

        for i in range(len(edges)):
            edges[i] = np.array([[np.random.randint(self.img_width), np.random.randint(self.img_height)] for _ in range(3)]).flatten()

        return edges

    
    def draw_triangles(self) -> None:
        self.content = Image.new(mode="RGB", size=(self.img_width, self.img_height))
        draw = ImageDraw.Draw(self.content, mode="RGBA")

        for rgba, edges in zip(self.triangle_colors, self.triangle_edges):
            draw.polygon(tuple(edges), fill=tuple(rgba))


    def mutate_triangles(self, mutate_rate: float) -> None:
        # Rows (a.k.a. triangles) that will mutate
        rgba_mask = np.random.choice([True, False], size=len(self.triangle_colors), p=[mutate_rate, 1 - mutate_rate]) 
        edge_mask = np.random.choice([True, False], size=len(self.triangle_edges), p=[mutate_rate, 1 - mutate_rate])

        for i, (mutate_rgba, mutate_edges) in enumerate(zip(rgba_mask, edge_mask)):
            if mutate_rgba:
                self.triangle_colors[i][0] = max(0, min(255, self.triangle_colors[i][0] + np.random.randint(-20, 20))) 
                self.triangle_colors[i][1] = max(0, min(255, self.triangle_colors[i][1] + np.random.randint(-20, 20))) 
                self.triangle_colors[i][2] = max(0, min(255, self.triangle_colors[i][2] + np.random.randint(-20, 20))) 
                self.triangle_colors[i][3] = max(80, min(204, self.triangle_colors[i][3] + np.random.randint(-5, 5))) 

            if mutate_edges:
                self.triangle_edges[i][0] = max(0, min(self.img_width, self.triangle_edges[i][0] + np.random.randint(-20, 20)))
                self.triangle_edges[i][1] = max(0, min(self.img_height, self.triangle_edges[i][1] + np.random.randint(-20, 20)))

                self.triangle_edges[i][2] = max(0, min(self.img_width, self.triangle_edges[i][2] + np.random.randint(-20, 20)))
                self.triangle_edges[i][3] = max(0, min(self.img_height, self.triangle_edges[i][3] + np.random.randint(-20, 20)))

                self.triangle_edges[i][4] = max(0, min(self.img_width, self.triangle_edges[i][4] + np.random.randint(-20, 20)))
                self.triangle_edges[i][5] = max(0, min(self.img_height, self.triangle_edges[i][5] + np.random.randint(-20, 20)))
