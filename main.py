#! /usr/bin/env python3

import glob
import sys
import os

import algorithm

def help_msg() -> str:
    return """Usage:
    ./main.py [PATH TO IMAGE] [IMAGES PER GENERATION] [MUTATION RATE]

Arguments:
    PATH TO IMAGE:          A relative path for an existing image to recreate
    IMAGES PER GENERATION:  The number of images produced per generation. Must be greater than 0.
    MUTATION RATE:          The percentage a triangle will mutate between generations. Values must be between 0 and 100."""


def main() -> None:
    if len(sys.argv) == 2 and (sys.argv[1] == "-h" or sys.argv[1] == "--help"):
        exit(help_msg())

    try:
        image_path = sys.argv[1]
        population = int(sys.argv[2])
        mutate_rate = float(sys.argv[3]) / 100

        if population <= 0 or (mutate_rate < 0 or mutate_rate > 100):
            raise ValueError

    except ValueError:
        exit("Invalid values entered for arguments.\nSee --help for guidance.")

    except IndexError:
        exit("Not enough arguments entered.\nSee --help for guidance.")

    if os.path.exists(image_path):
        if not os.path.exists("./out"):
            os.makedirs("./out")

        out_files = glob.glob("./out/*") 

        if len(out_files) != 0:
            response = input("There are files in the output directory. Do you really want to delete them? ")

            if response.lower()[0] == "y":
                for file in out_files:
                    os.remove(file)
            
            else:
                exit("Exiting...")
        
        algorithm.run(image_path, population, mutate_rate)

    else:
        exit(f"Source image does not exist at {image_path}\nExiting...")


if __name__ == "__main__":
    main()
